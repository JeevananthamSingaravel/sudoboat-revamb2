import React, { useEffect, useState } from "react";
import Menu from "@mui/material/Menu";
import MenuItem from "@mui/material/MenuItem";
import Link from "next/link";
import Button from "@mui/material/Button";
import { Typography } from "@mui/material";
import Avatar from "@mui/material/Avatar";
import Paper from "@mui/material/Paper";

async function getResponse(setNavContent) {
  const query = `query sudoboatNavBarEntryQuery {
    sudoboatNavBar(id: "32BBVqfPFjAVWIXlK8dHUp") {
       websiteTitle,
       demoTxt,
      sudoboatLogo{
        url
      },
      slugsCollection{
        items{
          slug,
          slugSubMenu,
          slugSubMenuImgCollection{
            items{
              url,
              title,
              description
            }
          }
        }
      }
    }
  }
  `;

  const res = await fetch(
    "https://graphql.contentful.com/content/v1/spaces/6h5tctqma4tw/environments/master?access_token=hPKw7_4S3nR921mUeLzueyk3lIpSX3qobifuyqwJvmk",
    {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({ query }),
    }
  );
  const post = await res.json();
  setNavContent(post?.data?.sudoboatNavBar);
}
export default function Navbar() {
  const [anchorEl, setAnchorEl] = useState(null);
  const [navContent, setNavContent] = useState(null);
  const [openMenu, setOpenMenu] = useState(null);
  const handleClick = (event, slug) => {
    setOpenMenu(slug);
    setAnchorEl(event?.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
    setOpenMenu(null);
  };

  useEffect(() => {
    getResponse(setNavContent);
  }, []);

  useEffect(() => {
    console.log(navContent, "navContent");
  }, [navContent]);
  return (
    <div position="static" className="navBar" style={{ width: "100%" }}>
      <div className="navBarContainer">
        <div className="logoContainer">
          <Avatar src={navContent?.sudoboatLogo?.url}></Avatar>
          <Link href={"/"} className="sudoboat">
            {navContent?.websiteTitle}
          </Link>
        </div>
        <div className="linkContainer">
          {navContent?.slugsCollection?.items?.map((data) => {
            return (
              <>
                <div>
                  <Typography key={data?.slug}>
                    <button
                      // href={`/${data.slug}`}
                      style={{ border: "none" }}
                      onClick={(e) => handleClick(e, data.slug)}
                    >
                      <div className="linkText">{data?.slug}</div>
                    </button>
                    <Paper open={openMenu && data.slug == openMenu}>
                      {data?.slugSubMenuImgCollection?.items?.length > 0 && (
                        <Menu
                          id="basic-menu"
                          anchorEl={anchorEl}
                          open={openMenu && data.slug == openMenu}
                          onClose={handleClose}
                          className={"menuBar"}
                          MenuListProps={{
                            "aria-labelledby": "basic-button",
                          }}
                          anchorOrigin={{
                            vertical: "bottom",
                            horizontal: "left",
                          }}
                          square={false}
                        >
                          {data?.slugSubMenuImgCollection?.items?.map(
                            (menuItem) => {
                              return (
                                menuItem?.title && (
                                  <MenuItem
                                    key={menuItem?.title}
                                    onClick={handleClose}
                                  >
                                    <div className="menuItemContainer">
                                      <Avatar src={menuItem?.url}></Avatar>
                                      <div className="menuItemTxtContainer">
                                        <div className="menuItemTitle">
                                          {menuItem?.title}
                                        </div>
                                        <div className={"menuItemDescription"}>
                                          {menuItem?.description}
                                        </div>
                                      </div>
                                    </div>
                                  </MenuItem>
                                )
                              );
                            }
                          )}
                        </Menu>
                      )}
                    </Paper>
                  </Typography>
                </div>
              </>
            );
          })}
          <Button style={{ color: "#1A54F2" }} className={"demoBtn"} onClick={()=>{window.scrollTo({ left: 0, top: document.body.scrollHeight, behavior: "smooth" })}}>
            {/* <Link href="/demo" className={"demoBtn"}> */}
              {navContent?.demoTxt}
            {/* </Link> */}
          </Button>
        </div>
      </div>
    </div>
  );
}
