import React from "react";
import Navbar from "./navbar";
import Home from "src/pages";
import Footer from "./footer";
import { useRouter } from "next/router";
const Layout = ({ children }) => {
  const routes = useRouter();
  return (
    <>
      <Navbar />
      <div style={{ width: "1440px", margin: "auto", paddingTop: "80px" }}>
        {children}
      </div>
      <Footer />
    </>
  );
};

Layout.propTypes = {};

export default Layout;
