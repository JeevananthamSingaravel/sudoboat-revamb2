import React, { useState, useEffect } from "react";
import styles from "./../styles/Index.module.css";
import Grid from "@mui/material/Grid";
import Paper from "@mui/material/Paper";
import ImageList from "@mui/material/ImageList";
import ImageListItem from "@mui/material/ImageListItem";
import { styled } from "@mui/material/styles";
import Card from "@mui/material/Card";
import CardHeader from "@mui/material/CardHeader";
import CardMedia from "@mui/material/CardMedia";
import CardContent from "@mui/material/CardContent";
import CardActions from "@mui/material/CardActions";
import Collapse from "@mui/material/Collapse";
import Avatar from "@mui/material/Avatar";
import IconButton from "@mui/material/IconButton";
import Typography from "@mui/material/Typography";
import { red } from "@mui/material/colors";
import FavoriteIcon from "@mui/icons-material/Favorite";
import ShareIcon from "@mui/icons-material/Share";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import MoreVertIcon from "@mui/icons-material/MoreVert";
const contentful = require("contentful");

const Item = styled(Paper)(({ theme }) => ({
  backgroundColor: theme.palette.mode === "dark" ? "#1A2027" : "#fff",
  ...theme.typography.body2,
  padding: theme.spacing(1),
  textAlign: "center",
  color: theme.palette.text.secondary,
}));

export function StandardImageList() {
  return (
    <ImageList sx={{ width: 500, height: 450 }} cols={3} rowHeight={164}>
      {itemData.map((item) => (
        <ImageListItem key={item.img}>
          <img
            src={`${item.img}?w=164&h=164&fit=crop&auto=format`}
            srcSet={`${item.img}?w=164&h=164&fit=crop&auto=format&dpr=2 2x`}
            alt={item.title}
            loading="lazy"
          />
        </ImageListItem>
      ))}
    </ImageList>
  );
}
const client = contentful.createClient({
  space: "6h5tctqma4tw",
  environment: "master", // defaults to 'master' if not set
  accessToken: "hPKw7_4S3nR921mUeLzueyk3lIpSX3qobifuyqwJvmk",
});

let response;
export const getStaticProps = async () => {
  await client
    .getEntry("1O98KJy6sfVkwlkR950tZG")
    .then((entry) => {
      // console.log(entry, "props");
      response = entry.fields;
    })
    .catch(console.error);
  // console.log(response, "respo");
  return {
    props: response,
  };
};

const ExpandMore = styled((props) => {
  const { expand, ...other } = props;
  return <IconButton {...other} />;
})(({ theme, expand }) => ({
  transform: !expand ? "rotate(0deg)" : "rotate(180deg)",
  marginLeft: "auto",
  transition: theme.transitions.create("transform", {
    duration: theme.transitions.duration.shortest,
  }),
}));
// const Home = (props) => {
//   const [expanded, setExpanded] = useState(false);

//   const handleExpandClick = () => {
//     setExpanded(!expanded);
//   };
//   return (
//     <>
//       {/* <Card className={styles.overAllContainer}> */}
//       <div className={styles.overAllContainer}>
//         {(props?.bodySection1Collection?.items).map((data, index) => {
//           return (
//             <>
//               <div style={{ display: "flex" }}>
//                 <div
//                   className={styles.bodyheader}
//                   style={{
//                     width:
//                       (data?.descriptionImagesCollection?.items).length > 0 &&
//                       "60%",
//                   }}
//                 >
//                   <span className={styles.bodyHeaderTxt}>{data.header}</span>
//                   <span
//                     className={styles.bodyDescription}
//                     style={{
//                       width:
//                         (data?.descriptionImagesCollection?.items).length > 0 &&
//                         "70%",
//                       paddingLeft: "20px",
//                     }}
//                   >
//                     {data.description?.map((data) => {
//                       return (
//                         <>
//                           <div>{data}</div>
//                         </>
//                       );
//                     })}
//                   </span>
//                 </div>

//                 {(data?.descriptionImagesCollection?.items).length > 0 &&
//                   (data?.descriptionImagesCollection?.items).map((data) => {
//                     return (
//                       <>
//                         <div className={styles.bodyDescriptionImg}>
//                           <ImageList
//                             sx={{ width: 500, height: 250 }}
//                             cols={3}
//                             rowHeight={100}
//                           >
//                             <ImageListItem key={data?.url}>
//                               <img
//                                 src={data?.url}
//                                 alt=""
//                                 style={{ width: "360px", height: "200px" }}
//                               ></img>
//                             </ImageListItem>
//                           </ImageList>
//                         </div>
//                       </>
//                     );
//                   })}
//               </div>
//             </>
//           );
//         })}
//         <div className={styles.cardContainer}>
//           {(props?.bodySection1CardCollection?.items).map((data) => {
//             return (
//               <>
//                 <Card sx={{ maxWidth: 345, padding: "5px" }}>
//                   <CardHeader
//                     avatar={
//                       <Avatar
//                         sx={{ width: "auto" }}
//                         src={data?.descriptionImages?.url}
//                       ></Avatar>
//                     }
//                     title={data?.header}
//                     // subheader="September 14, 2016"
//                   />
//                   <CardMedia
//                     component="img"
//                     height="194"
//                     image={data?.descriptionImage?.url}
//                     alt="Paella dish"
//                   />
//                   <CardContent>
//                     <Typography variant="body2" color="text.secondary">
//                       {data?.description}
//                     </Typography>
//                   </CardContent>
//                   {/* <CardActions disableSpacing>
//                   <ExpandMore
//                     expand={expanded}
//                     onClick={handleExpandClick}
//                     aria-expanded={expanded}
//                     aria-label="show more"
//                   >
//                     <ExpandMoreIcon />
//                   </ExpandMore>
//                 </CardActions>
//                 <Collapse in={expanded} timeout="auto" unmountOnExit>
//                   <CardContent>
//                     <Typography paragraph>Method:</Typography>
//                     <Typography paragraph>
//                       Heat 1/2 cup of the broth in a pot until simmering, add
//                       saffron and set aside for 10 minutes.
//                     </Typography>
//                     <Typography paragraph>
//                       Heat oil in a (14- to 16-inch) paella pan or a large, deep
//                       skillet over medium-high heat. Add chicken, shrimp and
//                       chorizo, and cook, stirring occasionally until lightly
//                       browned, 6 to 8 minutes. Transfer shrimp to a large plate
//                       and set aside, leaving chicken and chorizo in the pan. Add
//                       pimentón, bay leaves, garlic, tomatoes, onion, salt and
//                       pepper, and cook, stirring often until thickened and
//                       fragrant, about 10 minutes. Add saffron broth and
//                       remaining 4 1/2 cups chicken broth; bring to a boil.
//                     </Typography>
//                     <Typography paragraph>
//                       Add rice and stir very gently to distribute. Top with
//                       artichokes and peppers, and cook without stirring, until
//                       most of the liquid is absorbed, 15 to 18 minutes. Reduce
//                       heat to medium-low, add reserved shrimp and mussels,
//                       tucking them down into the rice, and cook again without
//                       stirring, until mussels have opened and rice is just
//                       tender, 5 to 7 minutes more. (Discard any mussels that
//                       don&apos;t open.)
//                     </Typography>
//                     <Typography>
//                       Set aside off of the heat to let rest for 10 minutes, and
//                       then serve.
//                     </Typography>
//                   </CardContent>
//                 </Collapse> */}
//                 </Card>
//               </>
//             );
//           })}
//         </div>
//       </div>
//       {/* </Card> */}
//     </>
//   );
// };

// export default Home;
// export const getStaticProps = async () => {
//   const query = `query bodySection1Collection {
//     bodySection1Collection {
//       items{
//         header,
//         description,descriptionImagesCollection{
//           items{
//             title,
//             description,
//             url
//           }
//         }
//       }
//     }
//     bodySection1CardCollection {
//       items {
//         header
//         description
//         descriptionImages{
//           url,
//           title,
//           description
//         }
//         descriptionImage{
//           url,
//           title,description
//         }
//       }
//     }
//   }`;
//   const res = await fetch(
//     "https://graphql.contentful.com/content/v1/spaces/6h5tctqma4tw/environments/master?access_token=hPKw7_4S3nR921mUeLzueyk3lIpSX3qobifuyqwJvmk",
//     {
//       method: "POST",
//       headers: {
//         "Content-Type": "application/json",
//       },
//       body: JSON.stringify({ query }),
//     }
//   );
//   const post = await res.json();
//   return {
//     props: post?.data,
//   };
// };

const Home = (props) => {
  const firstDescription = props?.description1;
  const SecondDescription = props?.description2;
  const thirdDescription = props?.description3;
  const cardDetails = props?.cardDetails;
  useEffect(() => {
    console.log(cardDetails, "firstDescription");
  }, [cardDetails]);

  return (
    <>
      <div className={styles.bodyConatiner}>
        <div className={styles.description1}>
          {firstDescription?.map((data) => {
            return (
              <>
                <Grid container spacing={2}>
                  <Grid item xs={6} md={7}>
                    <div className={styles.description1_header}>
                      {data?.fields?.header}
                    </div>
                    <div className={styles.description1_subHeader}>
                      {data?.fields?.subHeader}
                    </div>
                    <div className={styles.description1_description}>
                      <div>{data?.fields?.description}</div>
                    </div>
                  </Grid>
                  <Grid item xs={6} md={4}>
                    <div className={styles.description1_container2}>
                      <img
                        src={data?.fields?.mainImage?.fields?.file?.url}
                        alt=""
                        className={styles.description1_Image}
                      ></img>
                    </div>
                  </Grid>
                </Grid>
              </>
            );
          })}
        </div>
        <div className={styles.description2}>
          {SecondDescription?.map((logos) => {
            let fields = logos?.fields?.logos?.map((url) => {
              return (
                <>
                  <ImageListItem
                    key={url?.fields?.file?.url}
                    style={{ height: "100px" }}
                  >
                    <div className={styles.description2_images}>
                      <img
                        src={url?.fields?.file?.url}
                        alt=""
                        // style={{ width: "100%" }}
                      ></img>
                    </div>
                  </ImageListItem>
                </>
              );
            });
            return (
              <>
                <div className={styles.description2_imageContainer}>
                  <ImageList sx={{ width: "100%" }} cols={6} rowHeight={164}>
                    {fields}
                  </ImageList>
                </div>
              </>
            );
          })}
        </div>
        <div className={styles.description3}>
          {thirdDescription?.map((data, _x) => {
            console.log(data,"data",_x,"x")
            return (
              <>
              
                <Grid container spacing={2} style={{gap:"30px"}}>
               
                {data?.fields?.position === "left" && _x===0 && (
                    <>
                      <Grid item xs={2} md={5} className="body_Description3" style={{display:"flex",justifyContent:"flex-end"}}>
                        <div className={styles.description3_Section2} style={{width: "440px",height: "328px"}}>
                          <img
                            src={
                              data?.fields?.descriptionImages?.fields?.file?.url
                            }
                            alt=""
                            style={{height:"100%"}}
                          ></img>
                        </div>
                      </Grid>
                     
                      <Grid item xs={6} md={5} className="MuiGrid-itemDesc">
                        <div className={styles.description3_Section1}>
                          <div className={styles.description3_HeaderImg} >
                            <img
                              src={data?.fields?.headerImg?.fields?.file?.url}
                              alt=""
                            ></img>
                          </div>
                          <div style={{ width: "471px", fontSize: "18px",lineHeight: "27px"}}>
                            {data?.fields?.description}
                          </div>
                          {data?.fields?.descriptionTxt?.map((descriptions) => {
                            return (
                              <>
                                <div
                                  className={styles.description3_descriptions}
                                >
                                  <div>{descriptions}</div>
                                </div>
                              </>
                            );
                          })}
                        </div>
                      </Grid>
                    </>
                  )}
                
                </Grid>
              </>
            );
          })}
        </div>
        <div className={styles.cardContainer}>
          <div className={styles.separator}>
            <div className={styles.cordContainerDottedStyle}></div>
            {cardDetails?.map((cardDetails, _l) => {
              return (
                <>
                  <div className={styles.card}>
                    <div className={styles.particularCard}>
                      <div className={styles.imgContainer}>
                        <img
                          src={
                            cardDetails?.fields?.descriptionImages?.fields?.file
                              ?.url
                          }
                          alt=""
                        ></img>
                      </div>
                      <div className={styles.cardDescription}>
                        <div>{cardDetails?.fields?.description}</div>
                      </div>
                      <div className={styles.cardFooter}>
                        <div className={styles.cardFooterHead}>
                          {cardDetails?.fields?.cardFooterHead}
                        </div>
                        <div className={styles.cardFooterDescription}>
                          {cardDetails?.fields?.cardFooterDescription}
                        </div>
                      </div>
                    </div>
                  </div>
                </>
              );
            })}
          </div>
        </div>
        <div className={styles.description3}>
          {thirdDescription?.map((data, _x) => {
            console.log(data,"data",_x,"x")
            return (
              <>
              
                <Grid container spacing={2} style={{gap:"30px"}}>
                  {data?.fields?.position === "right" && (
                    <>
                      <Grid item xs={2} md={6} className="MuiGrid-itemDesc2">

                        <div className={styles.description3_Section1}>
                         
                          <div className={styles.description3_HeaderImg}>
                            <img
                              src={data?.fields?.headerImg?.fields?.file?.url}
                              alt=""
                            ></img>
                          </div>
                          <div style={{ width: "501px", fontSize: "18px",lineHeight: "27px"}}>
                            {data?.fields?.description}
                          </div>
                          {data?.fields?.descriptionTxt?.map((descriptions) => {
                            return (
                              <>
                                <div
                                  className={styles.description3_descriptions}
                                >
                                  {descriptions}
                                </div>
                              </>
                            );
                          })}
                        
                        </div>
                      </Grid>
                      <Grid item xs={6} md={5} style={{display:"flex",justifyContent:"flex-start"}}>
                        <div className={styles.description3_Section2}  style={{width: "440px",height: "328px"}}>
                          <img
                            src={
                              data?.fields?.descriptionImages?.fields?.file?.url
                            }
                            alt=""
                            style={{height:"100%"}}
                          ></img>
                        </div>
                      </Grid>
                    </>
                  )}
                   
                {data?.fields?.position === "left" && _x===2 && (
                    <>
                      <Grid item xs={2} md={5} className="body_Description3" style={{display:"flex",justifyContent:"flex-end"}}>
                        <div className={styles.description3_Section2} style={{width: "440px",height: "328px"}}>
                          <img
                            src={
                              data?.fields?.descriptionImages?.fields?.file?.url
                            }
                            alt=""
                            style={{height:"100%"}}
                          ></img>
                        </div>
                      </Grid>
                     
                      <Grid item xs={6} md={5} className="MuiGrid-itemDesc">
                        <div className={styles.description3_Section1}>
                          <div className={styles.description3_HeaderImg} >
                            <img
                              src={data?.fields?.headerImg?.fields?.file?.url}
                              alt=""
                            ></img>
                          </div>
                          <div style={{ width: "471px", fontSize: "18px",lineHeight: "27px"}}>
                            {data?.fields?.description}
                          </div>
                          {data?.fields?.descriptionTxt?.map((descriptions) => {
                            return (
                              <>
                                <div
                                  className={styles.description3_descriptions}
                                >
                                  <div>{descriptions}</div>
                                </div>
                              </>
                            );
                          })}
                        </div>
                      </Grid>
                    </>
                  )}
                </Grid>
              </>
            );
          })}
        </div>
      </div> 
    </>
  );
};
export default Home;
